// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Components/Widget.h"
#include "DUIRadioButtonController.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRadioButtonSelectionChanged_BP, UWidget*, SelectedWidget);

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class DEFAULTUI_API UDUIRadioButtonController : public UObject
{
	GENERATED_BODY()

	DECLARE_EVENT_OneParam(UDUIRadioButtonController, FOnItemSelected, UWidget*);
	
public:

	/** Unselect currently selected item if exists */
	UFUNCTION(BlueprintCallable)
	void UnselectCurrentlySelectedItem();

	/** Get all widgets being controlled*/
	UFUNCTION(BlueprintCallable)
	const TArray<UWidget*> & GetControlledWidgets() const;
	
	UFUNCTION(BlueprintCallable)
	void InitControlledWidgets(TArray<UWidget*> InControlledWidgets, int32 InSelectedRadioButton);

	/** Sets the item at the given index if index is valid */
	UFUNCTION(BlueprintCallable)
	void SetSelectedItemIndex(int32 Index);

	UFUNCTION(BlueprintCallable)
	void SetSelectedItem(UWidget * Item);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UWidget* GetSelectedItem() const;

	UFUNCTION(BlueprintCallable)
	void Reset();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsItemSelected(UWidget * Item) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsValidIndex(int32 ItemIndex) const;
	
	//UFUNCTION(BlueprintCallable)
	//void SetIsSelectedItemIndex(int32 Index, bool bIsSelected);

	
private:

	UFUNCTION() void OnItemClicked(UWidget * SelectedWidget);

	void BroadcastItemSelected(UWidget* ItemToBroadcast);

	void PostItemSelected();
	
public:
	FOnItemSelected OnItemSelected;



private:
	UPROPERTY(BlueprintAssignable, Category = Events, meta = (DisplayName = "On Item Selected"))
	FOnRadioButtonSelectionChanged_BP OnItemSelected_BP;
	
private:
	UPROPERTY() TArray<UWidget*> ControlledWidgets;

	UWidget* SelectedItem = nullptr;
};
