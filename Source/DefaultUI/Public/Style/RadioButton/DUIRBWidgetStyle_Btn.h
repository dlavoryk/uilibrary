// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SlateCore/Public/Styling/SlateTypes.h"
#include "DUIBaseRBWidgetStyle.h"

#include "DUIRBWidgetStyle_Btn.generated.h"

/**
 * @todo dlavoryk: consider adding customization for border
 */
UCLASS()
class DEFAULTUI_API UDUIRBWidgetStyle_Btn : public UDataAsset
{
	GENERATED_BODY()

public:


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= "Appearance|Button")
	FButtonStyle CheckedStyle;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= "Appearance|Button")
	FButtonStyle UncheckedStyle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Button")
	FLinearColor ButtonBGColor;

	/** The color of the text */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Appearance|Text", meta = (DisplayName = "Color And Opacity"))
	FSlateColor TextColorAndOpacity;
	
	/** The font to render the text with */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= "Appearance|Text")
	FSlateFontInfo Font;

	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Appearance)
	//FDUIBaseRBStyle SelectedStyle;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Appearance)
	//FDUIBaseRBStyle UncheckedStyle;

	
};
