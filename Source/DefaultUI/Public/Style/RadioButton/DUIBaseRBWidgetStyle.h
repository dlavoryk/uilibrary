// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyle.h"
#include "Styling/SlateWidgetStyleContainerBase.h"
#include "SlateCore/Public/Styling/SlateTypes.h"
#include "DUIBaseRBWidgetStyle.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct DEFAULTUI_API FDUIBaseRBStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FDUIBaseRBStyle();
	virtual ~FDUIBaseRBStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FDUIBaseRBStyle& GetDefault();

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Appearance)
	FButtonStyle ButtonStyle;
	
	/** Brush to drag as the background */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Appearance)
	FSlateBrush Brush;

	/** Color and opacity of the actual border image */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Appearance, meta = (sRGB = "true"))
	FLinearColor BrushColor;

	/** Color and opacity multiplier of content in the border */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content", meta = (sRGB = "true"))
	FLinearColor ContentColorAndOpacity;
	
	/** The padding area between the slot and the content it contains. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content")
	FMargin Padding;
	
};

/**
 */
UCLASS(BlueprintType, hidecategories=Object, MinimalAPI)
class UDUIBaseRBWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, BlueprintReadWrite, meta=(ShowOnlyInnerProperties))
	FDUIBaseRBStyle WidgetStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast< const struct FSlateWidgetStyle* >( &WidgetStyle );
	}
};
