// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Components/Widget.h"
#include "DUICheckBox.generated.h"

//enum class ECheckBoxState : unsigned char;
// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDUICheckBox : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DEFAULTUI_API IDUICheckBox
{
	GENERATED_BODY()


	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	DECLARE_EVENT_OneParam(IDUICheckBox, FOnItemClicked, UWidget*);


	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//void SetCheckedState(ECheckBoxState NewState);
	
	/*
	 * Change Item style depending on if it is selected or not
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, meta = (DisplayName = "Set Is Item Checked"))
	void SetIsItemChecked(bool IsChecked);

	/*
	 * Function is expected to receive "UWidget*" as param
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SubscribeItemClicked(UObject* Object, FName UFunctionName);
	
	/*
	 * Remove concrete function from execution
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveItemClickedDynamic(UObject* Object, FName UFunctionName);

	/*
	 * Remove all subscriptions from the object
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveItemClicked(UObject* Object);

};

