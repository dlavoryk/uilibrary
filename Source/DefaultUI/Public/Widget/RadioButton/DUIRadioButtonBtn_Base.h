// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DUICheckBox.h"
#include "DUIRadioButtonBtn_Base.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class DEFAULTUI_API UDUIRadioButtonBtn_Base : public UUserWidget, public IDUICheckBox
{
	GENERATED_BODY()


protected:
	/* Begin of UUserWidget Interface */
	virtual void NativePreConstruct() override;
	/* End of UUserWidget Interface */

	/* Begin of IDUICheckBox Interface */
	virtual void SetIsItemChecked_Implementation(bool IsChecked) override;
	virtual void SubscribeItemClicked_Implementation(UObject* Object, FName UFunctionName) override;
	virtual void RemoveItemClickedDynamic_Implementation(UObject* Object, FName UFunctionName) override;
	virtual void RemoveItemClicked_Implementation(UObject* Object) override;
	virtual void SubscribeItemClicked_Test_Implementation(FOnItemClicked ItemClicked) ;
	/* End of IDUICheckBox Interface */

	/*
	 * Change style of the widget
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ApplyStyle(bool IsChecked = false);

	/*
	 * Broadcast event that item was clicked
	 */
	UFUNCTION(BlueprintCallable)
	void BroadcastClicked();

	
protected:

	/*
	 * ///////////////////////////////////////
	 * Explanation ow chosen type:
	 * USlateWidgetStyleAsset was no used because "USlateWidgetStyleContainerBase" cannot be accessed in blueprints
	 * USlateWidgetStyleContainerBase was not used because there is no way to create it with no crutches in editor
	 * ////////////////////////////////////
	 *
	 * Style for RadioButton. DataAsset allows to use ANY style so you can customize whatever you want
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Style")
	class UDataAsset* RadioButtonStyle;

private:
	/** Event to notify about click */
	FOnItemClicked mOnItemClicked;

};
