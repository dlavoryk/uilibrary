// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Styling/SlateWidgetStyleContainerBase.h"
#include "DUIRadioButtonBtn_Base.h"
#include "DUIRadioButtonBtn.generated.h"

struct FDUIBaseRBStyle;
/**
 * 
 */
UCLASS()
class DEFAULTUI_API UDUIRadioButtonBtn : public UDUIRadioButtonBtn_Base
{
	GENERATED_BODY()


protected:
	/* Begin of UUserWidget Interface */
	virtual void NativeOnInitialized() override;
	
	//@todo dlavoryk: consider using this function if this is possible
	virtual void SynchronizeProperties() override;
	/* End of UUserWidget Interface */
	
	virtual void ApplyStyle_Implementation(bool IsChecked) override;

	
private:
	UFUNCTION() void OnButtonClicked();

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FText Text;
	
protected:
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* MyButton;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* MyTextBlock;
};
