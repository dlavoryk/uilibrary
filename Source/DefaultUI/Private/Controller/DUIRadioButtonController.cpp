// Fill out your copyright notice in the Description page of Project Settings.


#include "Controller/DUIRadioButtonController.h"
#include "DUICheckBox.h"
#include "Misc/AssertionMacros.h"
#include "Components/CheckBox.h"


void UDUIRadioButtonController::UnselectCurrentlySelectedItem()
{
	UWidget* CurSelectedWidget = GetSelectedItem();
	if (CurSelectedWidget)
	{
		IDUICheckBox::Execute_SetIsItemChecked(CurSelectedWidget, false);
		SelectedItem = nullptr;
	}
}

const TArray<UWidget*>& UDUIRadioButtonController::GetControlledWidgets() const
{
	return ControlledWidgets;
}

void UDUIRadioButtonController::InitControlledWidgets(TArray<UWidget*> InControlledWidgets, int32 InSelectedRadioButton)
{
	Reset();
	this->ControlledWidgets = MoveTemp(InControlledWidgets);
	for (UWidget * El : this->ControlledWidgets)
	{
		checkf(El->Implements<UDUICheckBox>() || Cast<IDUICheckBox>(El), TEXT("Widgets MUST implement IDUICheckBox or UDUICheckBox interface"));

		/*
		 * Apply default style
		 */
		IDUICheckBox::Execute_SetIsItemChecked(El, false);
		
		IDUICheckBox::Execute_SubscribeItemClicked(El, this, GET_FUNCTION_NAME_CHECKED(UDUIRadioButtonController, OnItemClicked));
	}
	SetSelectedItemIndex(InSelectedRadioButton);
	if (!GetSelectedItem())
	{
		SetSelectedItemIndex(0);
	}
}

void UDUIRadioButtonController::SetSelectedItemIndex(int32 Index)
{
	if (ensure(ControlledWidgets.IsValidIndex(Index)))
	{
		SelectedItem = ControlledWidgets[Index];
		PostItemSelected();
	}
	//else
	//{
	//	UnselectCurrentlySelectedItem();
	//}


	//SetIsSelectedItemIndex(Index, true);
}

void UDUIRadioButtonController::SetSelectedItem(UWidget* Item)
{
	check(Item != nullptr && ControlledWidgets.Find(Item) >= 0);
	SelectedItem = Item;
	PostItemSelected();
}

UWidget* UDUIRadioButtonController::GetSelectedItem() const
{
	return SelectedItem;
}

void UDUIRadioButtonController::Reset()
{
	for (UWidget * El : ControlledWidgets)
	{
		IDUICheckBox::Execute_RemoveItemClicked(El, this);
	}
	ControlledWidgets.Reset();
	SelectedItem = nullptr;
}

bool UDUIRadioButtonController::IsItemSelected(UWidget* Item) const
{
	return GetSelectedItem() == Item;
}

bool UDUIRadioButtonController::IsValidIndex(int32 ItemIndex) const
{
	return ControlledWidgets.IsValidIndex(ItemIndex);
}

void UDUIRadioButtonController::OnItemClicked(UWidget* SelectedWidget)
{
	if (!IsItemSelected(SelectedWidget))
	{
		UnselectCurrentlySelectedItem();
		SetSelectedItem(SelectedWidget);
	}
}

void UDUIRadioButtonController::BroadcastItemSelected(UWidget* ItemToBroadcast)
{
	OnItemSelected.Broadcast(ItemToBroadcast);
	OnItemSelected_BP.Broadcast(ItemToBroadcast);
}

void UDUIRadioButtonController::PostItemSelected()
{
	IDUICheckBox::Execute_SetIsItemChecked(SelectedItem, true);
	BroadcastItemSelected(SelectedItem);
}

//void UDUIRadioButtonController::SetIsSelectedItemIndex(int32 Index, bool bIsSelected)
//{
//	if (ensure(ControlledWidgets.IsValidIndex(Index)))
//	{
//		IDUICheckBox::Execute_SetIsChecked(ControlledWidgets[Index], bIsSelected);
//	}
//}
