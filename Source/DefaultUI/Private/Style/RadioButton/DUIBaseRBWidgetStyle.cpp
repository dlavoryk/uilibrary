// Fill out your copyright notice in the Description page of Project Settings.


#include "Style/RadioButton/DUIBaseRBWidgetStyle.h"

FDUIBaseRBStyle::FDUIBaseRBStyle()
{
}

FDUIBaseRBStyle::~FDUIBaseRBStyle()
{
}

const FName FDUIBaseRBStyle::TypeName(TEXT("FDUIBaseRBStyle"));

const FDUIBaseRBStyle& FDUIBaseRBStyle::GetDefault()
{
	static FDUIBaseRBStyle Default;
	return Default;
}

void FDUIBaseRBStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
	ButtonStyle.GetResources(OutBrushes);
	OutBrushes.Add(&Brush);
}

