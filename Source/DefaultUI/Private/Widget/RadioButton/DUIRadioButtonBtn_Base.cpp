// Fill out your copyright notice in the Description page of Project Settings.


#include "Widget/RadioButton/DUIRadioButtonBtn_Base.h"

void UDUIRadioButtonBtn_Base::NativePreConstruct()
{
	Super::NativePreConstruct();
#if WITH_EDITOR
	if (IsDesignTime())
	{
		ApplyStyle();
	}
#endif
}

void UDUIRadioButtonBtn_Base::SetIsItemChecked_Implementation(bool IsChecked)
{
	ApplyStyle(IsChecked);
}

void UDUIRadioButtonBtn_Base::SubscribeItemClicked_Implementation(UObject* Object, FName UFunctionName)
{
	mOnItemClicked.AddUFunction(Object, UFunctionName);
}

void UDUIRadioButtonBtn_Base::RemoveItemClickedDynamic_Implementation(UObject* Object, FName UFunctionName)
{
	//	mOnItemClicked.remo
	//	mOnItemClicked.AddUFunction(Object, UFunctionName);
	/*
	 * @todo dlavoryk: add implementation
	 */
	check(false);
}

void UDUIRadioButtonBtn_Base::RemoveItemClicked_Implementation(UObject* Object)
{
	mOnItemClicked.RemoveAll(Object);
}

void UDUIRadioButtonBtn_Base::SubscribeItemClicked_Test_Implementation(FOnItemClicked ItemClicked)
{
	mOnItemClicked = ItemClicked;
}

void UDUIRadioButtonBtn_Base::BroadcastClicked()
{
	mOnItemClicked.Broadcast(this);
}


void UDUIRadioButtonBtn_Base::ApplyStyle_Implementation(bool IsChecked)
{
	check(false && "ApplyStyle must be implemented in derived classes");

	check(false && "Do not call 'Super::' if u have already implemented");
}
