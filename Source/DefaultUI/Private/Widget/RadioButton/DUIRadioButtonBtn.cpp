// Fill out your copyright notice in the Description page of Project Settings.


#include "Widget/RadioButton/DUIRadioButtonBtn.h"
#include "Components/Button.h"
#include "Style/RadioButton/DUIBaseRBWidgetStyle.h"
#include "Style/RadioButton/DUIRBWidgetStyle_Btn.h"
#include "Components/Border.h"
#include "Components/TextBlock.h"


void UDUIRadioButtonBtn::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	MyButton->OnClicked.AddUniqueDynamic(this, &UDUIRadioButtonBtn::OnButtonClicked);
}

void UDUIRadioButtonBtn::SynchronizeProperties()
{
	//ApplyStyle();
	Super::SynchronizeProperties();
}

void UDUIRadioButtonBtn::ApplyStyle_Implementation(bool IsChecked)
{
	if (const auto MyStyle = Cast<UDUIRBWidgetStyle_Btn>(RadioButtonStyle))
	{
		MyButton->SetStyle(IsChecked ? MyStyle->CheckedStyle : MyStyle->UncheckedStyle);
		MyButton->SetBackgroundColor(MyStyle->ButtonBGColor);

		MyTextBlock->SetColorAndOpacity(MyStyle->TextColorAndOpacity);
		MyTextBlock->SetFont(MyStyle->Font);
		
		MyTextBlock->SetText(Text);
	}
}

void UDUIRadioButtonBtn::OnButtonClicked()
{
	BroadcastClicked();
}